# -*- coding: utf-8 -*-

import pytest
from dm_verity.skeleton import fib

__author__ = "Marcin Cieślak"
__copyright__ = "Marcin Cieślak"
__license__ = "gpl3"


def test_fib():
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
